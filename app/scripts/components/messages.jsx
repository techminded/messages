import React from 'react'
import Router from 'react-router';
import MessageStore from "../stores/messageStore";
import MessageActions from "../actions/messageActions";
import AuthStore from '../stores/authStore';

import moment from "moment";
import unescape from "unescape";

import { Link } from 'react-router';

let PAGE_LIMIT = 3;

var Messages = React.createClass({

  mixins: [Router.Navigation],

  getInitialState() {
    return {
      messages: MessageStore.getMessages(),
      currentUser: AuthStore.currentUser,
      pages: 1
    }
  },

  componentDidMount() {
    this.setState({
      messages: MessageStore.getMessages(),
      currentUser: AuthStore.currentUser,
      pages: 1
    });
    document.body.className = document.body.className + ' background-dark';
    this.unsubscribe = MessageStore.listen(this.onMessageStoreChange.bind(this));
    this.unsubscribeAuth = AuthStore.listen(this.onAuthChange.bind(this));
    MessageActions.loadMessages();
  },

  componentWillUnmount() {
    this.unsubscribe();
    this.unsubscribeAuth();
    document.body.className =
        document.body.className.replace(/\bbackground-dark\b/,'');
  },

  onMessageStoreChange(data) {
    if (data.messages) {
      if (this.state.currentUser && Object.keys(this.state.currentUser).length > 0) {
        data.messages = data.messages.filter(function(message) {
          return message.author == this.state.currentUser.email
        }.bind(this));
      }
      data.messages.reverse();
    }
    this.setState(data);
  },

  onDeleteItemClick(messageId) {
    MessageActions.deleteMessage(messageId);
  },

  onAuthChange(state) {
    if (! state.authorized) {
      this.transitionTo('signin');
    }
  },

  onMoreClick(event) {
    this.setState({
      pages: this.state.pages + 1
    });
  },

  render() {
    var moreContents = function() {
      if (this.state.messages.length > (PAGE_LIMIT * this.state.pages)) {
        return (<div>
          <div className="more-link" onClick={this.onMoreClick.bind(this)}>
            <img src="/images/next.svg"/>
          </div>
          <div className="newemessage-bottom-link">
            <Link to="messages-new">
              <img src="/images/edit.svg"/>
            </Link>
          </div>
        </div>)
      } else {
        return '';
      }
    }.bind(this);
    var count = 0;

    return (
      <div className="messages">
        {this.state.messages.map(function(message) {
          count = count + 1;
          if (count > PAGE_LIMIT * this.state.pages) {
            return '';
          }
          var time = moment(message.datetime).format('H:mm');
          var text = unescape(message.text);
          return (
              <div className="messages__item-container">
                <div className="messages__item__close" onClick={this.onDeleteItemClick.bind(this, message.id)}>
                  <img src="/images/close_white.svg" />
                </div>
                <div className="messages__item">
                    <div className="messages__item__time">
                      {time}
                    </div>
                    <div className="messages__item__text">
                      {text}
                    </div>
                 </div>
              </div>
          )
        }.bind(this))}
        {moreContents()}
      </div>
    );
  }

});

export default Messages;