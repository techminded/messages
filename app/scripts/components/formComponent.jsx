import React from 'react';

class FormComponent extends React.Component {
  
  constructor(){
    super();
  }

  componentWillMount() {
    this.errors = {};
  }

  hasErrors() {
    return Object.keys(this.errors).length > 0;
  }

  errorStyleClass(name) {
    return this.errors.hasOwnProperty(name) ? 'error' : '';
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  valueLink(name) {
    var fn = this['on' + this.capitalizeFirstLetter(name) + 'Change'];
    return {
      value: this.state[name],
      requestChange: fn.bind(this)
    };

  }
}

export default FormComponent;