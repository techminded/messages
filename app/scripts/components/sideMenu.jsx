import React from 'react';
import { Link } from 'react-router';

import SideMenuActions from "../actions/sideMenuActions";

class SideMenu extends React.Component {

  constructor(props, context) {
    super(props);

  }

  onCloseClick() {
    SideMenuActions.close();
  }

  render() {
    return (
      <div className="sidemenu">
        <div className="button-close" onClick={this.onCloseClick.bind(this)}>
          <img src="/images/close_white.svg" />
        </div>
        <ul>
          <li><Link to="messages-my">My Messages</Link></li>
          <li><Link to="messages-new">New message</Link></li>
          <li><Link to="settings">Settings</Link></li>
        </ul>
      </div>
    );
  }

}

export default SideMenu;