import React from 'react';

import SideMenu from "../components/sideMenu.jsx";
import SideMenuStore from "../stores/sideMenuStore";
import AuthStore from "../stores/authStore";
import SideMenuActions from "../actions/sideMenuActions";
import AuthActions from "../actions/authActions";

import { Link } from 'react-router';

class Header extends React.Component{

  constructor(props, context) {
    super(props);
    this.state = {
      sideMenuOpened: false,
      currentUser: AuthStore.currentUser
    };
  }
  componentDidMount() {
    this.unsubscribeSide = SideMenuStore.listen(this.onSideMenuChange.bind(this));
    this.unsubscribeAuth = AuthStore.listen(this.onAuthChange.bind(this));

  }

  componentWillUnmount() {
    this.unsubscribeSide();
    this.unsubscribeAuth();
  }

  onSideMenuChange(state) {
    this.setState({
      sideMenuOpened: state.opened
    })
  }

  onAuthChange(state) {
    this.setState({
      currentUser: state.currentUser
    })
  }

  onMenuOpenClick() {
    SideMenuActions.open();
    this.setState({
      sideMenuOpened: true
    });
  }

  onSignOutClick() {
    AuthActions.signOut();
  }

  render() {
    var authorized = this.state.currentUser
        && Object.keys(this.state.currentUser).length > 0;
    var authContents = function() {
      if (authorized) {
        var user = this.state.currentUser;
        return (
            <div>
              <div>
                <img className="side-menu" src="/images/menu.svg" onClick={this.onMenuOpenClick.bind(this)} />
              </div>
              {this.state.sideMenuOpened ? <SideMenu/> : ''}
              <div className="userbox">
                <span className="username">
                  {user.firstName + " " + user.lastName}
                </span>
                <img src="/images/logout.png" onClick={this.onSignOutClick.bind(this)} />
              </div>
            </div>
        );
      } else {
        return <p></p>;
      }
    }.bind(this);
    return (
      <header className="clearfix">
        {authContents()}
      </header>
    );
  }

}

export default Header;