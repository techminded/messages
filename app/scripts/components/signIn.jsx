import React from 'react';
import AuthActions from '../actions/authActions';
import FormComponent from "../components/formComponent.jsx";

import AuthStore from '../stores/authStore';

class SignIn extends FormComponent {
  
  constructor(props){
    super(props);
    this.state = {
      email: '',
      password: '',
      error: ''
    };

  }

  componentDidMount() {
    this.unsubscribe = AuthStore.listen(this.onStatusChange.bind(this));
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onStatusChange(state) {
    this.setState(state);
  }

  onEmailChange(emailValue) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (! re.test(emailValue)) {
      this.errors['email'] = 'Email is invalid';
    } else {
      delete this.errors['email'];
    }
    this.setState({ email: emailValue });
  }

  onPasswordChange(passwordValue) {
    if (! (passwordValue && passwordValue.length > 3)) {
      this.errors['password'] = 'Password is invalid';
    } else {
      delete this.errors['password'];
    }
    this.setState({ password: passwordValue });
  }

  onClick(event) {
    if (this.hasErrors()) {
      alert('Fix errors first pls');
    } else {
      AuthActions.signIn(this.state.email, this.state.password);
    }
  }

  render() {
    var errorContents = this.state.error != '' ? <div className="error-general">{this.state.error}</div> : '';
    return (
      <div>
        <div className="signup-mark"><img src="/images/mark.png" /></div>
        <ul className="main-menu signup">
          {errorContents}
          <li className="main-menu__email">
            <div className="contents">
              <input type="text" placeholder="Email" className={this.errorStyleClass('email')} valueLink={this.valueLink('email')} />
            </div>
          </li>
          <li className="main-menu__password">
            <div className="contents">
              <input type="text" placeholder="Password" className={this.errorStyleClass('password')} valueLink={this.valueLink('password')} />
            </div>
          </li>
          <li role="button" className="main-menu__create" onClick={this.onClick.bind(this)}>
            <div className="contents">Sign In</div>
          </li>
        </ul>
      </div>
    );
  }
}

export default SignIn;