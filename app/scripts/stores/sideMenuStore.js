import Reflux from 'reflux';
import SideMenuActions from '../actions/sideMenuActions';
import Cookies from 'cookies-js';


var SideMenuStore = Reflux.createStore({

  init() {
    this.opened = false;

    this.listenTo(SideMenuActions.open, this.open);
    this.listenTo(SideMenuActions.close, this.close);

  },

  open() {
    this.trigger({
      opened: true
    });
  },

  close() {
    this.trigger({
      opened: false
    });
  }

});

export default SideMenuStore;