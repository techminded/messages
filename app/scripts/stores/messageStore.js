import Reflux from 'reflux';
import MesssageActions from '../actions/messageActions';

var MessageStore = Reflux.createStore({

  init() {
    this.messages = [];

    this.listenTo(MesssageActions.loadMessages, this.loadMessages);
    this.listenTo(MesssageActions.loadMessagesSuccess, this.loadMessagesSuccess);
    this.listenTo(MesssageActions.loadMessagesError, this.loadMessagesError);
    this.listenTo(MesssageActions.submitMessage, this.submitMessage);
  },

  getMessages() {
    return this.messages;
  },

  loadMessages() {
    this.trigger({ 
      loading: true
    });
  },

  loadMessagesSuccess(messages) {
    this.messages = messages;

    this.trigger({
      messages : this.messages,
      loading: false
    });
  },

  loadMessagesError(error) {
    this.trigger({ 
      error : error,
      loading: false
    });
  },

  submitMessage(messageText, author) {

  }
});

export default MessageStore;