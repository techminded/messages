import Reflux from 'reflux';
import AuthActions from '../actions/authActions';
import Cookies from 'cookies-js';
import constants from '../constants';


var AuthStore = Reflux.createStore({

  init() {
    this.authorized = Cookies.get(constants.AUTH_COOKIE_NAME);

    this.listenTo(AuthActions.signUp, this.signUp);
    this.listenTo(AuthActions.signIn, this.signIn);
    this.listenTo(AuthActions.signOut, this.signOut);
    this.listenTo(AuthActions.update, this.update);

    this.users = localStorage.users ? JSON.parse(localStorage.users) : {};
    this.currentUser = this.authorized ? this.users[Cookies.get(constants.USER_ID_COOKIE_NAME)] : {};

    this.trigger({
      authorized: this.authorized,
      currentUser: this.currentUser
    });
  },

  signIn(email, password) {
    var authorized = false, currentUser = {}, error = false;
    if (this.users[email] != undefined && this.users[email].password == password) {
      authorized = true;
      currentUser = this.users[email];
      Cookies.set(constants.AUTH_COOKIE_NAME, authorized);
      Cookies.set(constants.USER_ID_COOKIE_NAME, email);
    } else {
      error = 'Wrong email or password';
    }
    this.trigger({
      authorized: authorized,
      currentUser: currentUser,
      error: error
    });
  },

  signUp(firstName, lastName, email, password) {
    if (email != '' && password != '') {
      if (this.users[email] != undefined) {
        this.trigger({
          authorized: false,
          currentUser: {},
          error: 'User already exists'
        });
        return false;
      }
      this.users[email] = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password
      };
      localStorage.users = JSON.stringify(this.users);
      this.signIn(email, password);
    } else {
      this.trigger({
        authorized: false,
        currentUser: {},
        error: 'Wrong data provided'
      });
    }

  },

  update(firstName, lastName, email, password) {
    if (this.users[email] != undefined) {
      this.users[email] = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password
      };
      localStorage.users = JSON.stringify(this.users);
    }
  },

  signOut() {
    Cookies.expire(constants.AUTH_COOKIE_NAME);
    Cookies.expire(constants.USER_ID_COOKIE_NAME);
    this.trigger({
      authorized: false,
      currentUser: {}
    });
  }

});


export default AuthStore;