import Reflux from 'reflux';

var AuthActions = Reflux.createActions([
  'signUp',
  'signIn',
  'signOut',
  'update'
]);

export default AuthActions;