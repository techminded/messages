import Reflux from 'reflux';
import moment from 'moment';
import Guid from 'guid';

var MessageActions = Reflux.createActions([
  'loadMessages',
  'loadMessagesSuccess',
  'loadMessagesError',
  'submitMessage',
  'deleteMessage'
]);

MessageActions.loadMessages.preEmit = function(data){
  // make your api call/ async stuff here
  // we use setTimeout for faking async behaviour here
  setTimeout(function(){
    var messages = localStorage.messages ? JSON.parse(localStorage.messages) : [];
    MessageActions.loadMessagesSuccess(messages);

    // on error
    // ItemActions.loadItemsError('an error occured');
  },500);
};

MessageActions.submitMessage.preEmit = function(messageText, author){

  var messages = localStorage.messages ? JSON.parse(localStorage.messages) : [];
  messages.push({ id: Guid.raw(), author: author, datetime: moment(), text: messageText});
  localStorage.messages = JSON.stringify(messages);
  MessageActions.loadMessages();

};

MessageActions.deleteMessage.preEmit = function(messageId){

  var messages = localStorage.messages ? JSON.parse(localStorage.messages) : [];
  messages = messages.filter(function(message) {
    return message.id != messageId;
  });
  localStorage.messages = JSON.stringify(messages);
  MessageActions.loadMessages();

};

export default MessageActions;