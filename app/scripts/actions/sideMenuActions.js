import Reflux from 'reflux';

var SideMenuActions = Reflux.createActions([
  'open',
  'close'
]);

export default SideMenuActions;