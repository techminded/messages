import React from 'react';
import { Route, DefaultRoute, NotFoundRoute } from 'react-router';

import App from './pages/app.jsx';
import Home from './pages/home.jsx';
import SignUpPage from './pages/signUpPage.jsx';
import SignInPage from './pages/signInPage.jsx';
import Settings from './pages/settings.jsx';
import Messages from './components/messages.jsx';
import MessagesMy from './pages/messagesMy.jsx';
import MessagesNew from './pages/messagesNew.jsx';
import NotFound from './pages/notFound.jsx';

var routes = (
  <Route name="app" path="/" handler={ App }>
    <Route name="signup" handler={ SignUpPage } />
    <Route name="signin" handler={ SignInPage } />
    <Route name="home" handler={ Home } />
    <Route name="messages" path="messages">
      <Route name="messages-my" path="my" handler={ MessagesMy } />
      <Route name="messages-new" path="new" handler={ MessagesNew } />
      <DefaultRoute handler={ Messages } />
    </Route>
    <Route name="settings" handler={ Settings } />
    <DefaultRoute handler={ Home } />
    <NotFoundRoute handler={ NotFound } />
  </Route>
);

export default routes;