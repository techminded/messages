import React from 'react';

import Messages from "../components/messages.jsx";
import MessageActions from "../actions/messageActions";
import escape from "escape";
import AuthStore from '../stores/authStore';

import { Link } from 'react-router';

class MessagesNew extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      symbolsNumber: 0,
      text: '',
      currentUser: AuthStore.currentUser
    }
  }

  getInitialState() {
    return {
      symbolsNumber: 0,
      text: '',
      currentUser: AuthStore.currentUser
    }
  }

  componentDidMount() {
    this.unsubscribe = AuthStore.listen(this.onAuthChange.bind(this));
    this.setState({
      symbolsNumber: 0
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onAuthChange(state) {
    this.setState(state);
  }

  onTextChange(text) {
    this.setState({
      symbolsNumber: text.length,
      text: text
    });
  }

  onSubmitClick(event) {
    if (this.state.text != '') {
      MessageActions.submitMessage(escape.execute(this.state.text, {type: 'html'}), this.state.currentUser.email);
      this.setState({
        symbolsNumber: 0,
        text: ''
      });
    }
  }

  render() {
    var textValueLink = {
      value: this.state.text,
      requestChange: this.onTextChange.bind(this)
    };
    return (
      <div>
        <h1 className="page-title">New message</h1>
        <div className="messages-editor">
          <div className="messages-editor__close">
            <Link to="messages-my">
              <img src="/images/close.svg" />
            </Link>
          </div>
          <textarea className="messages-editor__textarea" rows="5" valueLink={textValueLink}></textarea>
          <div className="messages-editor__counter">{this.state.symbolsNumber}</div>
          <div className="messages-editor__submit" onClick={this.onSubmitClick.bind(this)}>
            <img src="/images/add.svg" />
          </div>
        </div>
        <Messages />
      </div>
    );
  }

}

export default MessagesNew;