import React from 'react';
import AuthActions from '../actions/authActions';

import AuthStore from '../stores/authStore';

import Router from 'react-router';

var SignUpPage = React.createClass({

  mixins: [Router.Navigation],

  componentWillMount() {
    this.errors = {};
    this.state = {
      firstname: '',
      lastname: '',
      email: '',
      password: '',
      error: '',
      errors: ''
    };
  },
  getInitialState() {
    return {
      firstname: '',
      lastname: '',
      email: '',
      password: '',
      error: '',
      errors: ''
    }
  },
  componentDidMount() {
    this.unsubscribe = AuthStore.listen(this.onStatusChange.bind(this));
  },

  componentWillUnmount() {
    this.unsubscribe();
  },

  hasErrors() {
    return Object.keys(this.errors).length > 0;
  },

  errorStyleClass(name) {
    return this.errors.hasOwnProperty(name) ? 'error' : '';
  },

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  },

  valueLink(name) {
    var fn = this['on' + this.capitalizeFirstLetter(name) + 'Change'];
    return {
      value: this.state[name],
      requestChange: fn.bind(this)
    };

  },

  onStatusChange(state) {
    if (state.authorized) {
      this.transitionTo('home');
    }
    this.setState(state);
  },

  onEmailChange(emailValue) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (! re.test(emailValue)) {
      this.errors['email'] = 'Email is invalid';
    } else {
      delete this.errors['email'];
    }
    this.setState({ email: emailValue });
  },

  onPasswordChange(passwordValue) {
    if (! (passwordValue && passwordValue.length > 3)) {
      this.errors['password'] = 'Password is invalid';
    } else {
      delete this.errors['password'];
    }
    this.setState({ password: passwordValue });
  },

  onFirstnameChange(firstnameValue) {
    this.setState({ firstname: firstnameValue });
  },

  onLastnameChange(lastnameValue) {
    this.setState({ lastname: lastnameValue });
  },

  onClick(event) {
    if (this.hasErrors()) {
      alert('Fix errors first pls');
    } else {
      AuthActions.signUp(this.state.firstname, this.state.lastname, this.state.email, this.state.password);
    }
  },

  render() {
    var errorContents = this.state.error != '' ? <div className="error-general">{this.state.error}</div> : '';
    return (
        <div>
          <h1 className="page-title">Wellcome</h1>
          <ul className="main-menu">
            {errorContents}
            <li className="main-menu__firstname">
              <div className="contents">
                <input type="text" placeholder="First Name" className={this.errorStyleClass('firstname')} valueLink={this.valueLink('firstname')} />
              </div>
            </li>
            <li className="main-menu__lastname">
              <div className="contents">
                <input type="text" placeholder="Last Name" className={this.errorStyleClass('lastname')} valueLink={this.valueLink('lastname')} />
              </div>
            </li>
            <li className="main-menu__email">
              <div className="contents">
                <input type="text" placeholder="Email" className={this.errorStyleClass('email')} valueLink={this.valueLink('email')} />
              </div>
            </li>
            <li className="main-menu__password">
              <div className="contents">
                <input type="text" placeholder="Password" className={this.errorStyleClass('password')} valueLink={this.valueLink('password')} />
              </div>
            </li>
            <li role="button" className="main-menu__signup" onClick={this.onClick.bind(this)}>
              <div className="contents">Create Account</div>
            </li>
          </ul>
        </div>
    );
  }
});

export default SignUpPage;