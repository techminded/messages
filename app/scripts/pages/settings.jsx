import React from 'react';
import AuthActions from '../actions/authActions';
import AuthStore from "../stores/authStore";
import FormComponent from "../components/formComponent.jsx";

class Settings extends FormComponent {

  constructor(props){
    super(props);
    var currentUser = AuthStore.currentUser;
    this.state = {
      firstname: currentUser.firstName,
      lastname: currentUser.lastName,
      email: currentUser.email,
      password: currentUser.password,
      error: ''
    };

  }

  getInitialState() {
    return {
      firstname: currentUser.firstName,
      lastname: currentUser.lastName,
      email: currentUser.email,
      password: currentUser.password,
      error: ''
    }
  }

  onEmailChange(emailValue) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (! re.test(emailValue)) {
      this.errors['email'] = 'Email is invalid';
    } else {
      delete this.errors['email'];
    }
    this.setState({ email: emailValue });
  }

  onPasswordChange(passwordValue) {
    this.setState({ password: passwordValue });
  }

  onFirstnameChange(firstnameValue) {
    this.setState({ firstname: firstnameValue });
  }

  onLastnameChange(lastnameValue) {
    this.setState({ lastname: lastnameValue });
  }

  onClick(event) {
    if (this.hasErrors()) {
      alert('Fix errors first pls');
    } else {
      AuthActions.update(this.state.firstname, this.state.lastname, this.state.email, this.state.password);
    }
  }

  render() {
    var errorContents = this.state.error != '' ? <div className="error-general">{this.state.error}</div> : '';
    return (
        <div>
          <h1 className="page-title">Settings</h1>
          <ul className="main-menu">
            {errorContents}
            <li className="main-menu__firstname">
              <div className="contents">
                <input type="text" placeholder="First Name" className={this.errorStyleClass('firstname')} valueLink={this.valueLink('firstname')} />
              </div>
            </li>
            <li className="main-menu__lastname">
              <div className="contents">
                <input type="text" placeholder="Last Name" className={this.errorStyleClass('lastname')} valueLink={this.valueLink('lastname')} />
              </div>
            </li>
            <li className="main-menu__email">
              <div className="contents">
                <input type="text" placeholder="Email" className={this.errorStyleClass('email')} valueLink={this.valueLink('email')} />
              </div>
            </li>
            <li className="main-menu__password">
              <div className="contents">
                <input type="text" placeholder="Password" className={this.errorStyleClass('password')} valueLink={this.valueLink('password')} />
              </div>
            </li>
            <li role="button" className="main-menu__create" onClick={this.onClick.bind(this)}>
              <div className="contents">Save</div>
            </li>
          </ul>
        </div>
    );
  }
}

export default Settings;