import React from 'react';
import AuthStore from '../stores/authStore';

import SignIn from '../components/signIn.jsx';
import Header from '../components/header.jsx';
import Messages from '../components/messages.jsx';

import { Link } from 'react-router';

var Home = React.createClass({

  getInitialState() {
    return {
      authorized: AuthStore.authorized
    }
  },

  componentDidMount() {
    this.state = {
      authorized: AuthStore.authorized
    };
    this.unsubscribe = AuthStore.listen(this.onStatusChange.bind(this));
  },

  componentWillUnmount() {
    this.unsubscribe();
  },

  onStatusChange(state) {
    console.log('onStatusChange', state);
    this.setState(state);
  },

  render() {
    if (this.state.authorized) {
      return (
          <div>
            <h1 className="page-title">Messages</h1>
            <div className="newmessage-top-link">
              <Link to="messages-new">
                <img src="/images/edit.svg" />
              </Link>
            </div>
            <Messages />
          </div>
      )
    } else {
      return (
        <div>
          <SignIn />
          <div className="bottom-link">
            <Link to="signup">Create Account</Link>
          </div>
        </div>
      );
    }
  }
});

export default Home;