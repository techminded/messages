import React from 'react';
import { RouteHandler } from 'react-router';
import Header from '../components/header.jsx'

import AuthStore from '../stores/authStore';

import Router from 'react-router';

var App = React.createClass({

  mixins: [Router.Navigation],

  render() {
    return (
      <div>
        <Header />
        <div className="content">
          <RouteHandler/>
        </div>
      </div>
    );
  }

});

export default App;