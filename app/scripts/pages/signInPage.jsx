import React from 'react';
import SignIn from "../components/signIn.jsx";

import AuthStore from '../stores/authStore';

import { Link } from 'react-router';

import Router from 'react-router';

var SignInPage  = React.createClass({

  mixins: [Router.Navigation],

  componentDidMount() {
    this.unsubscribe = AuthStore.listen(this.onAuthChange.bind(this));
  },

  componentWillUnmount() {
    this.unsubscribe();
  },

  onAuthChange(state) {
    if (state.authorized) {
      this.transitionTo('home');
    }
  },

  render() {
    return (
      <div>
        <div>
          <SignIn />
          <div className="bottom-link">
            <Link to="signup">Create Account</Link>
          </div>
        </div>
      </div>
    );
  }
});

export default SignInPage;