import React from 'react';

import Messages from '../components/messages.jsx';

import { Link } from 'react-router';

class MessagesMy extends React.Component {

  render() {
    return (
        <div>
            <h1 className="page-title">Messages</h1>
            <div className="newmessage-top-link">
                <Link to="messages-new">
                    <img src="/images/edit.svg" />
                </Link>
            </div>
            <Messages />
        </div>
    );
  }

}

export default MessagesMy;